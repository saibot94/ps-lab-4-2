#include "language.h"

string current_lang = "en";

map<string, map<string, string>* >* languages;
map<string, string> * read_language_file(string lang_name);

void change_language(string lang_code) {
    if(languages == NULL) {
        languages =
            new map<string, map<string, string>* >();
    }
    if(languages->find(lang_code) ==languages->end()) {
        (*languages)[lang_code] =
            read_language_file(lang_code);
       }
    current_lang = lang_code;
}


string translate(string word_code) {
    map<string,string>* lang =
        languages->find(current_lang)->second;
    return lang->find(word_code)->second;
}

map<string, string> * read_language_file
(string lang_name)
{
    map<string, string>* lang = new map<string, string>();
    string full_name = lang_name + string(".txt");
    ifstream f(full_name.c_str());
    string line;
    while(!f.eof()) {
        getline(f, line);
        int position = line.find("=");
        if(position != string::npos){
            string key = line.substr(0, position);
            string value = line.substr(position+1);
            (*lang)[key] = value;
        }

    }
    f.close();
    return lang;


}
