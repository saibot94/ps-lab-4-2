#ifndef SUBMENUITEM_H
#define SUBMENUITEM_H

#include "projectlibs.h"
#include "MenuItem.h"
#include "language.h"

class SubmenuItem: public MenuItem
{
    vector<MenuItem*>& menu_items;
    private:
        bool valid_option(int i);

    public:
        SubmenuItem(string name, vector<MenuItem*>& menu_items_param)
        : MenuItem(name), menu_items(menu_items_param) {}
        /* TODO
            Implement this function in the SubmenuItem.cpp file.

            The function should read an option from the user (index of the next option to select)
            and trigger the do_stuff function on it, until the user chooses 'Back' and is taken to the parent menu.

            The menu should redraw if the user is trying to choose an option that doesn't exist.

            Example.

        */
        virtual bool do_stuff();
};
#endif // SUBMENUITEM_H
